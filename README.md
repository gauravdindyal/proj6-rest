Author: Gaurav Dindyal
Email: gauravd@uoregon.edu
Description:
All implementation of listing times are added with json and csv. 

ListAll prints both open and close, and the option of listOpenOnly

 or close to choose whether you want to view open or close only.

use the following ports:
5000 is the index,
5001 is used to view specified lists,
5003 is consumer's input.
The programs needs an .ini file with specific data.